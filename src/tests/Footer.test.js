import { render, screen } from "@testing-library/react";
import Footer from "../components/Footer";
import { BrowserRouter, Switch, Route } from "react-router-dom";

test("footer is showing", () => {
  render(
    <BrowserRouter>
      <Footer />
    </BrowserRouter>
  );
  const copyrightText = screen.getByText("Copyright © 2021 by CB");
  expect(copyrightText).toBeInTheDocument();
});

test("link WeatherApi is showing", () => {
  render(
    <BrowserRouter>
      <Footer />
    </BrowserRouter>
  );
  const linkApi = screen.getByTestId("weatherApi");
  expect(linkApi).toBeTruthy();
});

test("link WeatherApi is navigating correctly", () => {
  render(
    <BrowserRouter>
      <Switch>
        <Route exact path="/weatherApi">
          weather Api page
        </Route>
        <Footer />
      </Switch>
    </BrowserRouter>
  );
  const linkApi = screen.getByTestId("weatherApi");
  linkApi.click();
  const weatherApiPage = screen.getByText("weather Api page");
  expect(weatherApiPage).toBeInTheDocument();
});
