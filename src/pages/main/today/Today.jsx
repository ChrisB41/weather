import React, { useState, useMemo } from "react";
import TodayWeatherCard1 from "./TodayWeatherCard1";
import "./Today.css";
import Select from "react-select";
import countryList from "react-select-country-list";
import { FaSearch } from "react-icons/fa";
import { Spinner } from "react-bootstrap";
import FetchToday from "../../../components/fetch/FetchToday";

const Today = () => {
  const { data, error, isLoading } = FetchToday;
  console.log(data);
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const options = useMemo(() => countryList().getData(), []);
  const changeHandler = (country) => {
    setCountry(country);
  };
  const getError = () => {
    if (error) {
      return <h2 style={{ marginLeft: 400 }}>Error when fetching: {error}</h2>;
    }
  };
  return (
    <div className="weather">
      <div className="search-today">
        <div className="search-box-today">
          <input
            className="search-bar-today"
            type="text"
            placeholder="Search City..."
            onChange={(event) => setCity(event.target.value)}
            value={city}
          />{" "}
        </div>
        <div className="search-box-today">
          <Select
            placeholder="Search Country..."
            options={options}
            value={country}
            className="search-bar-today"
            onChange={changeHandler}
          />
          <button className="fa" onClick={FetchToday}>
            <FaSearch />
          </button>
        </div>
      </div>
      <div>
        {isLoading && (
          <div>
            {" "}
            <Spinner animation="border" className="air-spinner" />
          </div>
        )}
        {/*{isLoading !== true && typeof data.main != "undefined" ? (
          <TodayWeatherCard1 weatherData={data} refresh={FetchToday} />
        ) : (
          <div></div>
        )}*/}
        {getError()}
      </div>
    </div>
  );
};

export default Today;
