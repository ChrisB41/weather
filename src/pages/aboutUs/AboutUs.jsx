import React from "react";
import "./AboutUs.css";

const AboutUs = () => {
  return (
    <div className="about">
      <h1 style={{ marginTop: "100px", marginBottom: "30px" }}>About Us</h1>
      <div className="containerItem">
        <img src="earth.jpg" alt="earth" className="full" />
        <p className="big">An Alternative Outlook</p>
        <p>
          Weather Underground has challenged the conventions around how weather
          information is shared with the public since 1993. We're immensely
          proud of the unique products that our community and meteorologists
          have created to improve people's access to meaningful weather data
          from around the globe. As the Internet's 1st weather service, we
          consider ourselves pioneers within our field and we're constantly
          seeking new data sets and the next technologies that will help us
          share more data with more people.
        </p>
      </div>
      <div className="container">
        <div className="containerItem">
          <img src="aboutRoof.jpg" alt="roof" />
          <p className="big"> Weather For All</p>
          <p>
            {" "}
            Our brand mission is to make quality weather information available
            to every person on this planet. No matter where you live in the
            world or how obscure an activity you require weather information for
            - we will provide you with as much relevant, local weather data as
            we can uncover. We promise to provide weather data for those that
            are underserved by other weather providers.
          </p>
        </div>
        <div className="containerItem">
          <img src="aboutReport.jpg" alt="report" />
          <p className="big">Together We Know More</p>
          <p>
            The beating heart of our brand is the generous and passionate
            community of weather enthusiasts that share weather data and content
            across our products. With 250,000+ of our members sending real-time
            data from their own personal weather stations, they provide us with
            the extensive data that makes our forecasts and products so unique.
          </p>
        </div>
        <div className="containerItem">
          <img src="aboutInstall.jpg" alt="install" />
          <p className="big">Community Backed by Science </p>
          <p>
            {" "}
            The vast amount of weather data we collect only becomes meaningful
            when combined with the scientific expertise that our team of
            meteorologists provide. Our proprietary forecast model leverages our
            personal weather station community to provide the most reliable and
            localized forecasts available. Our meteorologists and climatologists
            also provide valuable insight into the science behind the data and
            the relationship between weather and climate change.
          </p>
        </div>
      </div>
      <div className="containerItem">
        <img src="aboutCollage.jpg" alt="collage" className="full" />
        <p className="big">Data as Design</p>
        <p>
          Especially in a world where so much data is consumed on the small
          screen of a mobile phone, we have invested a lot of time and energy
          into the design of our products across all digital platforms. We have
          created a user experience that enables you to find as much weather
          data as you need as quickly as possible.
        </p>
      </div>
    </div>
  );
};
export default AboutUs;
